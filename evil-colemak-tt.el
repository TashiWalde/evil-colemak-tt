;;; evil-colemak-tt.el --- Evil-keybindings for the colemak++ keyboard layout.

;; Copyright 2021 Tashi Walde <tashi.walde@gmail.com>

;; Author: Tashi Walde <tashi.walde@gmail.com>
;; Package-Requires: ((evil "20170323.1140"))

;; This file is NOT part of GNU Emacs.

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; This package is heavily inspired by colemak-evil by Patrick Brinich-Langlois,
;; found at https://github.com/patbl/colemak-evil.

;;; Code:
(use-package 'evil)


(set-in-navigation-evil-states "b" (lambda () (interactive) (insert "test")))


(provide 'evil-colemak-tt)

;;; evil-colemak-tt.el ends here
